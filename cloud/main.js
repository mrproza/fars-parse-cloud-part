// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("printToConsole", function(request, response) {
	console.log("LOG printToConsole: " + request.params.message);
  response.success("Hello world!");
});

//Need this function to verify if we hav this student already
Parse.Cloud.beforeSave("Student", function(request) {
	
});

//SAVING NEW STUDENT
Parse.Cloud.afterSave("Student", function(request) {
	
	//1. Create record in Presence if Start date is before Today
	//var todayDate = moment();
	//var startDate = moment(request.object.get("startDate"));
	//console.log("LOG todayDate: " + todayDate.format("dddd, MMMM Do YYYY"));
	//console.log("LOG startDate: " + startDate.format("dddd, MMMM Do YYYY"));

	var Presence = Parse.Object.extend("Presence");

	var startDate = new Date(request.object.get("startDate")).setHours(0,0,0,0);
	console.log("LOG startDate: " + startDate);
	var todayDate = new Date().setHours(0,0,0,0); //Current date in seconds; this is not Date object; it's Int
	console.log("LOG todayDate: " + todayDate);
	var dayLength = 86400000; //number of milliseconds in a day

	
	for (var dayi = startDate; dayi <= todayDate; dayi += dayLength){
		console.log("LOG dayi: " + dayi);
		console.log("LOG new Date(dayi): " + new Date(dayi));
		
		presence = new Presence();
		presence.set("date", new Date(dayi));
		presence.set("studentName", request.object.get("FirstName"));
		presence.set("studentId", request.object.id);
		presence.set("startDate", request.object.get("startDate"));
		presence.setACL(new Parse.ACL(Parse.User.current()));
		
		
		
		
		
		presence.save(null, {
			  success: function(presence) {
				// Execute any logic that should take place after the object is saved.
				//alert('New object created with objectId: ' + presence.id);
				console.log("LOG student: " + request.object.get("FirstName") + "; date: " + new Date(dayi));
			  },
			  error: function(presence, error) {
				// Execute any logic that should take place if the save fails.
				// error is a Parse.Error with an error code and description.
				alert('Failed to create new object, with error code: ' + error.description);
			  }
		});	
	}
});
